require_relative './spec_helper'
require_relative '../src/05_unit_testing'

describe Numero do
  describe 'constructor' do
    it 'debería inicializar el valor correctamente' do
      numero = Numero.new(10)

      expect(numero.valor).to eq(10)
    end
  end

  describe '#sumar' do
    it 'debería sumar dos números correctamente' do
      numero1 = Numero.new(10)
      numero2 = Numero.new(20)

      resultado = numero1.sumar(numero2)

      expect(resultado.valor).to eq(30)
    end

    # TODO
  end

  describe '#restar' do
    it 'debería restar dos números correctamente' do
      numero1 = Numero.new(50)
      numero2 = Numero.new(10)

      resultado = numero1.restar(numero2)

      expect(resultado.valor).to eq(40)
    end

    # TODO
  end

  describe '#mayor?' do
    # TODO
  end
end
