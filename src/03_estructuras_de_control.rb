class EjemplosEstructurasDeControl
  def ejemplos_condiciones
    puts "--- Ejemplos de if"
    if 1 < 2
      puts "1 es menor que 2"
    end
    if 1 > 2
      puts "1 es mayor que 2"
    else
      puts "1 no es mayor que 2"
    end
    if 1 > 2
      puts "1 es mayor que 2"
    elsif 1 < 2
      puts "1 es menor que 2"
    else
      puts "1 es igual a 2"
    end

    puts "--- Ejemplos de condiciones con operadores lógicos"
    if 1 < 2 && 2 < 3
      puts "1 es menor que 2 Y 2 es menor que 3"
    end
    if 1 < 2 || 2 > 3
      puts "1 es menor que 2 O 2 es mayor que 3"
    end

    puts "--- Ejemplo de unless"
    unless 1 > 2
      puts "1 no es mayor que 2"
    end

    puts "--- Ejemplo de case"
    case 5
    when 1
      puts "Es 1"
    when 2
      puts "Es 2"
    when 3
      puts "Es 3"
    else
      puts "Es otro"
    end

    puts "-- Ejemplo de condiciones sobre valores no booleanos"
    if 0
      puts "0 es verdadero"
    else
      puts "0 es falso"
    end
    if 1
      puts "1 es verdadero"
    else
      puts "1 es falso"
    end
    if ''
      puts "'' es verdadero"
    else
      puts "'' es falso"
    end
    if 'hola'
      puts "'hola' es verdadero"
    else
      puts "'hola' es falso"
    end
    if nil
      puts "nil es verdadero"
    else
      puts "nil es falso"
    end
  end

  def ejemplos_ciclos
    puts "--- Ejemplo de for con un rango"
    for i in 1..5
      puts i
    end

    puts "--- Ejemplo de for con un array"
    for i in [6, 7, 8, 9, 10]
      puts i
    end

    puts "--- Ejemplo de each"
    ['a', 'b', 'c', 'd', 'e'].each do |i|
      puts i
    end

    puts "--- Ejemplo de while"
    i = 0
    while i < 5
      puts i
      i += 1
    end
  end
end

obj = EjemplosEstructurasDeControl.new

puts '----------------------------'
puts "Ejemplos de condiciones:"
obj.ejemplos_condiciones
puts '----------------------------'

# puts "Ejemplos de ciclos:"
# obj.ejemplos_ciclos
# puts '----------------------------'
