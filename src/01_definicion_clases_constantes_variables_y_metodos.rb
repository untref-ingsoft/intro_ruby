class Foo
  UNA_CONSTANTE = 'Soy una constante'

  attr_accessor :atributo_publico

  def initialize(atributo1, atributo2)
    @atributo1 = atributo1
    @atributo2 = atributo2

    @atributo_publico = 'Hola, soy publico'
    @otro_atributo_publico = 'Hola, yo tambien soy publico!'
  end

  # Getter y Setter para atributo (equivalente a attr_accessor)
  def otro_atributo_publico
    @otro_atributo_publico
  end

  def otro_atributo_publico=(valor)
    @otro_atributo_publico = valor
  end

  def metodo_1(parametro1, parametro2)
    puts "Dentro de metodo_1: parametro1 = #{parametro1}, parametro2 = #{parametro2}"
  end

  def metodo_2
    una_variable_local = 'Soy una variable local!'
    otra_variable_local = 99
  end

  def prueba_de_valor_de_retorno_con_return
    entero_1 = 10
    entero_2 = 50

    return entero_1 + entero_2
  end

  def prueba_de_valor_de_retorno_sin_return
    entero_1 = 30
    entero_2 = 40

    entero_1 + entero_2
  end

  def cambiar_la_constante
    # UNA_CONSTANTE = 'Otro valor desde un método'
  end
end

# Creando una instancia de la clase Foo
foo = Foo.new('aaa', 123)
puts foo.inspect

# Accediendo a los atributos de la instancia
# puts "El valor de atributo_publico es: #{foo.atributo_publico}"
# foo.atributo_publico = 'Otro valor para el atributo publico'
# puts "Ahora el valor de atributo_publico es: #{foo.atributo_publico}"
# # puts foo.atributo1 # Esto falla porque el atributo es privado
# puts "El valor de otro_atributo_publico es: #{foo.otro_atributo_publico}"
# foo.otro_atributo_publico = 'Otro valor para el otro atributo publico'
# puts "Ahora el valor de otro_atributo_publico es: #{foo.otro_atributo_publico}"

# # Llamando metodos con parámetros
# foo.metodo_1('foo', 15.9)
# foo.metodo_1([1, 2, 3], {a: 'foo', 56 => ['a', 'b']})

# # Probando las variables locales
# foo.metodo_2
# foo.inspect # No se ven las variables locales una_variable_local y otra_variable_local

# # Probando el valor de retorno
# puts foo.prueba_de_valor_de_retorno_con_return
# puts foo.prueba_de_valor_de_retorno_sin_return

# # Probando constantes
# puts Foo::UNA_CONSTANTE
# Foo::UNA_CONSTANTE = 'Otro valor' # Esto va a mostrar un warning porque estamos "redefiniendo" la constante
# puts Foo::UNA_CONSTANTE
# foo.cambiar_la_constante # Esto falla porque estamos intentando cambiar el valor de una constante
