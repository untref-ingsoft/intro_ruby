require 'debug'

class Foo
  def initialize(un_parametro, otro_parametro)
    debugger
  end

  def metodo(el_parametro)
    debugger
  end
end

obj = Foo.new('aaaaaaa', [ 1, 2, 3 ])

obj.metodo({ a: '1', b: '2', c: '3' })
