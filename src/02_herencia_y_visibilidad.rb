class Base
  attr_accessor :atributo

  def initialize(valor_atributo)
    @atributo = valor_atributo
  end

  def metodo_0
    puts "En metodo_0 de Base"
  end

  def metodo_1
    puts "En metodo_1 de Base"
  end

  protected

  def metodo_protegido
    puts "En metodo_protegido"
  end

  private

  def metodo_privado
    puts "En metodo_privado"
  end
end

class SubClase < Base
  def metodo_1
    puts "En metodo_1 de SubClase"
    super
  end

  def metodo_2
    puts "En metodo_2 de SubClase"
  end

  def llamar_metodo_protegido_de_este_objeto
    puts "Llamando al metodo protegido de self..."
    metodo_protegido
  end

  def llamar_metodo_protegido_de_otro_objeto(otro_objeto)
    puts "Llamando al metodo protegido de otro_objeto..."
    otro_objeto.metodo_protegido
  end

  def llamar_metodo_privado(otro_objeto)
    puts "Llamando al metodo privado de self..."
    metodo_privado
    puts "Llamando al metodo privado de otro_objeto..."
    otro_objeto.metodo_privado
  end
end

obj = SubClase.new('Hola mundo')
puts obj.inspect

# puts '----------------------------'
# puts "El valor del atributo es: #{obj.atributo}"
# puts '----------------------------'

# obj.metodo_0
# puts '----------------------------'

# obj.metodo_1
# puts '----------------------------'

# obj.metodo_2
# puts '----------------------------'

# # Metodos protegidos vs. privados
# obj.llamar_metodo_protegido_de_este_objeto
# obj.llamar_metodo_protegido_de_otro_objeto(SubClase.new(1))
# puts '----------------------------'

# obj.metodo_protegido
# puts '----------------------------'

# obj.llamar_metodo_privado(SubClase.new(1))
