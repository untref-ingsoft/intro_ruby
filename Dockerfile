FROM ruby:3.3.4-alpine3.20

# Instalación de las dependencias:

# 1- Instala las depedendencias nativas
RUN apk update && apk add --no-cache build-base
# 2- Crea subdirectorio con las dependencias
RUN mkdir setup
# 3- Copia la definición de las dependencias
COPY  Gemfile /setup
COPY  Gemfile.lock /setup
# 4- Instala las dependencias
WORKDIR /setup
RUN bundle install

# Creo el subdirectorio de trabajo sobre el que se monta el volumen al correr el contenedor
WORKDIR /workspace

CMD ["/bin/sh"]
